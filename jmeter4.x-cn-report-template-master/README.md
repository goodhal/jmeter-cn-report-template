## 将report-template目录替换apache-jmeter-4.x\bin\report-template目录即可
下载：https://gitee.com/smooth00/jmeter-cn-report-template/attach_files/270893/download
         
## 可以写个bat或sh进行html报告批量生成

目前是UTF-8 - 无BOM 格式，这样可以同时兼容Windows和Linux下的中文显示
如果生成的html报告显示中文乱码，自行将模板文件转存合适的编码格式
